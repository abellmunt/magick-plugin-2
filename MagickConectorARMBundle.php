<?php

declare(strict_types=1);

namespace MauticPlugin\MagickConectorARMBundle;

use Mautic\IntegrationsBundle\Bundle\AbstractPluginBundle;

class MagickConectorARMBundle extends AbstractPluginBundle
{
}
