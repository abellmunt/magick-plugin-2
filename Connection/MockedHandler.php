<?php

declare(strict_types=1);

namespace MauticPlugin\MagickConectorARMBundle\Connection;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Promise;
use GuzzleHttp\Promise\PromiseInterface;

use function GuzzleHttp\Psr7\parse_query;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Util\Json;
use Psr\Http\Message\RequestInterface;

class MockedHandler extends MockHandler
{
    ////////////////////////////////////////////////////////////
    ////////MODIFICAR LA URL PARA EL CLIENTE QUE QUERAMOS///////
    ////////////////////////////////////////////////////////////
    private function getUrl(){
        $url = 'https://sales.magickhub.com';
        return $url;
    }
    ////////////////////////////////////////////////////////////

    public function __invoke(RequestInterface $request, array $options): PromiseInterface
    {
        return $this->getResponse($request);
    }

    private function getResponse(RequestInterface $request): PromiseInterface
    {
        $path   = $request->getUri()->getPath();
        $method = $request->getMethod();

        switch ($path) {
            case '/api/fields/contact':
                return $this->getFields('contact');
            case '/api/fields/company':
                return $this->getFields('company');
        }

        throw new \Exception(sprintf('%s is not supported for method %s', $path, $method));
    }

    private function getFields(string $object): PromiseInterface
    {
        $url = $this->getUrl();
        $urlToken = $url . '/Api/access_token';
        $token = $this->getToken($urlToken);
        $defaultFields = [
            'id',
            'lifecycle_stage_c',
            'email1',
            'first_name',
            'last_name',
            'phone_work',
            'phone_mobile',
            'first_campaign_c',
            'first_content_c',
            'first_medium_c',
            'first_source_c',
            'last_campaign_c',
            'last_content_c',
            'last_medium_c',
            'last_source_c'
        ];

         if($object == 'contact'){
            $url = $url . '/Api/V8/meta/fields/Leads';
            $curl = curl_init($url);

            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $headers = array(
                'Authorization: Bearer ' . $token['access_token'],
            );
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            $respLeads = curl_exec($curl);
            curl_close($curl);

            $decodeLeadFields = json_decode($respLeads, true);

            $arrayLeadsFields = [];

            foreach($decodeLeadFields['data']['attributes'] as $key => $fieldLead){
                if(!in_array($key, $defaultFields)){
                    array_push($arrayLeadsFields, array(
                        'name' => $key,
                        'label' => $key,
                        'data_type' => $fieldLead['type'],
                        'required' => $fieldLead['required'],
                        'writable' => true
                    ));
                }
            }

            $leadFieldsJson = sprintf(json_encode($arrayLeadsFields));

            return Promise\Create::promiseFor(new Response(
                200,
                ['Content-Type' => 'application/json; charset=UTF-8'],
                $leadFieldsJson
            ));
         } else if($object == 'company') {
            return Promise\Create::promiseFor(new Response(
                200,
                ['Content-Type' => 'application/json; charset=UTF-8'],
                ''
            ));
         }
    }

    function getToken($url)
    {
        $curl = curl_init($url);
        $data = 'grant_type=client_credentials&client_id=1adea3f9-df14-7daa-6de9-61f26c9dffaf&client_secret=EpKMTi7$$uKg';
        $headers = array(
            "Content-Type: application/x-www-form-urlencoded",
         );

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $resp = curl_exec($curl);
        curl_close($curl);
        $respDecode = json_decode($resp, true);
        return $respDecode;
    }
}
