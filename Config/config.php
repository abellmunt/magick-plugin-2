<?php

declare(strict_types=1);

return [
    'name'        => 'Magick Conector ARM',
    'description' => 'Conector de Magick entre las herramientas de ARM',
    'version'     => '1.0.0',
    'author'      => 'Increnta',
    'routes'      => [
        'main'   => [],
        'public' => [],
        'api'    => [],
    ],
    'menu'        => [],
    'services'    => [
        'other'        => [
            // Provides access to configured API keys, settings, field mapping, etc
            'magickconector.config'            => [
                'class'     => \MauticPlugin\MagickConectorARMBundle\Integration\Config::class,
                'arguments' => [
                    'mautic.integrations.helper',
                ],
            ],
            // Configuration for the http client which includes where to persist tokens
            'magickconector.connection.config' => [
                'class'     => \MauticPlugin\MagickConectorARMBundle\Connection\Config::class,
                'arguments' => [
                    'mautic.integrations.auth_provider.token_persistence_factory',
                ],
            ],
            // The http client used to communicate with the integration which in this case uses OAuth2 client_credentials grant
            'magickconector.connection.client' => [
                'class'     => \MauticPlugin\MagickConectorARMBundle\Connection\Client::class,
                'arguments' => [
                    'mautic.integrations.auth_provider.oauth2twolegged',
                    'magickconector.config',
                    'magickconector.connection.config',
                    'monolog.logger.mautic',
                ],
            ],
        ],
        'sync'         => [
            // Returns available fields from the integration either from cache or "live" via API
            'magickconector.sync.repository.fields'      => [
                'class'     => \MauticPlugin\MagickConectorARMBundle\Sync\Mapping\Field\FieldRepository::class,
                'arguments' => [
                    'mautic.helper.cache_storage',
                    'magickconector.connection.client',
                ],
            ],
            // Creates the instructions to the sync engine for which objects and fields to sync and direction of data flow
            'magickconector.sync.mapping_manual.factory' => [
                'class'     => \MauticPlugin\MagickConectorARMBundle\Sync\Mapping\Manual\MappingManualFactory::class,
                'arguments' => [
                    'magickconector.sync.repository.fields',
                    'magickconector.config',
                ],
            ],
            // Proxies the actions of the sync between Mautic and this integration to the appropriate services
            'magickconector.sync.data_exchange' => [
                'class'     => \MauticPlugin\MagickConectorARMBundle\Sync\DataExchange\SyncDataExchange::class,
                'arguments' => [
                    'magickconector.sync.data_exchange.report_builder',
                    'magickconector.sync.data_exchange.order_executioner',
                ],
            ],
            // Builds a report of updated and new objects from the integration to sync with Mautic
            'magickconector.sync.data_exchange.report_builder' => [
                'class'     => \MauticPlugin\MagickConectorARMBundle\Sync\DataExchange\ReportBuilder::class,
                'arguments' => [
                    'magickconector.connection.client',
                    'magickconector.config',
                    'magickconector.sync.repository.fields',
                ],
            ],
            // Pushes updated or new Mautic contacts or companies to the integration
            'magickconector.sync.data_exchange.order_executioner' => [
                'class'     => \MauticPlugin\MagickConectorARMBundle\Sync\DataExchange\OrderExecutioner::class,
                'arguments' => [
                    'magickconector.connection.client',
                ],
            ],
        ],
        'integrations' => [
            // Basic definitions with name, display name and icon
            'mautic.integration.magickconector'               => [
                'class' => \MauticPlugin\MagickConectorARMBundle\Integration\MagickConectorIntegration::class,
                'tags'  => [
                    'mautic.integration',
                    'mautic.basic_integration',
                ],
            ],
            // Provides the form types to use for the configuration UI
            'magickconector.integration.configuration' => [
                'class'     => \MauticPlugin\MagickConectorARMBundle\Integration\Support\ConfigSupport::class,
                'arguments' => [
                    'magickconector.sync.repository.fields',
                ],
                'tags'      => [
                    'mautic.config_integration',
                ],
            ],
            // Defines the mapping manual and sync data exchange service for the sync engine
            'magickconector.integration.sync'          => [
                'class'     => \MauticPlugin\MagickConectorARMBundle\Integration\Support\SyncSupport::class,
                'arguments' => [
                    'magickconector.sync.mapping_manual.factory',
                    'magickconector.sync.data_exchange',
                ],
                'tags'      => [
                    'mautic.sync_integration',
                ],
            ],
        ],
        'events' => [
            'magickconector.stats.subscriber' => [
                'class'     => \MauticPlugin\MagickConectorARMBundle\EventListener\StatsSubscriber::class,
                'arguments' => [
                    'mautic.security',
                    'doctrine.orm.entity_manager',
                ],
            ],
        ],
    ],
];
